#!/bin/sh

cat trace_slabinfo.txt | grep drm_i915 | awk "{print $1}" &> output_drm_i915_gem_request.log
cat trace_meminfo.txt | grep MemFree | awk '{print $1,$2}' | less &> output_MemFree.log
cat trace_meminfo.txt | grep MemAvailable | awk '{print $1,$2}' | less  &> output_MemAvailable.log
cat trace_meminfo.txt | grep Cached | awk '{print $1,$2}' | less &> output_Cached.log
cat trace_meminfo.txt | grep Buffers | awk '{print $1,$2}' | less &> output_Buffers.log
cat trace_meminfo.txt | grep Slab | awk '{print $1,$2}' | less &> output_Slab.log

sed '/SwapCached/d' output_Cached.log > output_Cached2.log

echo "done transfer_data"
