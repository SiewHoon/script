#!/bin/sh

#export LD_LIBRARY_PATH="/opt/intel/mediasdk/samples/_bin/x64"
export LD_LIBRARY_PATH="/opt/intel/mediasdk/samples/__cmake/intel64.make.debug/__bin/debug"

rm -rf /home/root/dump/trace_*.txt
rm -rf /home/root/dump/output__*.log

#CASE="1920_816_hw_vaapi"
#CASE="1920_816"
#CASE="960_540_hw_vaapi"
CASE="960_540"

WIDTH=960
HEIGHT=540

j='0'
#MSDK_SAMPLE_PATH="/opt/intel/mediasdk/samples/_bin/x64"
MSDK_SAMPLE_PATH="/opt/intel/mediasdk/samples/__cmake/intel64.make.debug/__bin/debug"

rm  $MSDK_SAMPLE_PATH/output_*.log

for i in {1..1000}
do 
   
   echo "data collect $i"
   # cat /sys/kernel/debug/dri/0/i915_gem_objects >> /home/root/dump/trace_i915.txt
   cat /proc/slabinfo >> /home/root/dump/trace_slabinfo.txt
   cat /proc/meminfo >> /home/root/dump/trace_meminfo.txt
   # cat /proc/vmstat >> /home/root/dump/trace_vmstat.txt
   sleep 1

   if [ $i -eq 100 ]
   then
       (( ++j ))
       echo "play video $j"
       #echo $MSDK_SAMPLE_PATH
       cd $MSDK_SAMPLE_PATH
       ./sample_decode h264 -w $WIDTH -h $HEIGHT -i /home/root/Batman_1080p.264 -o /dev/null &> output_sample_decode_"$CASE"_"$j".log &
   fi

   if [ $i -eq 300 ]
   then
       (( ++j ))
       echo "play video $j"
       #echo $MSDK_SAMPLE_PATH
       cd $MSDK_SAMPLE_PATH
       ./sample_decode h264 -w $WIDTH -h $HEIGHT -i /home/root/Batman_1080p.264 -o /dev/null &> output_sample_decode_"$CASE"_"$j".log &
   fi

   if [ $i -eq 500 ]
   then
       (( ++j ))
       echo "play video $j"
       #echo $MSDK_SAMPLE_PATH
       cd $MSDK_SAMPLE_PATH
       ./sample_decode h264 -w $WIDTH -h $HEIGHT -i /home/root/Batman_1080p.264 -o /dev/null &> output_sample_decode_"$CASE"_"$j".log &
   fi

done

echo "finished collect data"

